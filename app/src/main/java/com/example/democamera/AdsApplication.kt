package com.example.democamera

import android.app.Application
import android.util.Log
import com.appsflyer.AppsFlyerLib
import com.appsflyer.adrevenue.AppsFlyerAdRevenue
import com.appsflyer.api.PurchaseClient
import com.appsflyer.api.Store
import com.appsflyer.internal.models.InAppPurchaseValidationResult
import com.appsflyer.internal.models.SubscriptionValidationResult

class AdsApplication : Application(){
    override fun onCreate() {
        super.onCreate()
        var appsFlyerKey = "KeyAF"
        var roi360 = true
        initAFTracking(appsFlyerKey,roi360)
    }
    private fun initAFTracking(appsFlyerKey: String, roi360:Boolean?) {
        // init and start the native AppsFlyer Core SDK
        if (appsFlyerKey.isNotEmpty()) {
            AppsFlyerLib.getInstance().init(appsFlyerKey, null, this)
            AppsFlyerLib.getInstance().start(this)
            AppsFlyerLib.getInstance().setDebugLog(true)
            val afRevenueBuilder: AppsFlyerAdRevenue.Builder = AppsFlyerAdRevenue.Builder(this)
            AppsFlyerAdRevenue.initialize(afRevenueBuilder.build())
        }
        // init - Make sure to save a reference to the built object. If the object is not saved,
        // it could lead to unexpected behavior and memory leaks.
        if (roi360!=null&& roi360){
            val afPurchaseClient = PurchaseClient.Builder(applicationContext, Store.GOOGLE)
                // Enable Subscriptions auto logging
                .logSubscriptions(true)
                // Enable In Apps auto logging
                .autoLogInApps(true)
                // set production environment
                .setSandbox(false)
                // Subscription Purchase Event Data source listener. Invoked before sending data to AppsFlyer servers
                // to let customer add extra parameters to the payload
                .setSubscriptionPurchaseEventDataSource {
                    mapOf(
                        "some key" to "some value",
                        "another key" to it.size
                    )
                }
                // In Apps Purchase Event Data source listener. Invoked before sending data to AppsFlyer servers
                // to let customer add extra parameters to the payload
                .setInAppPurchaseEventDataSource {
                    mapOf(
                        "some key" to "some value",
                        "another key" to it.size
                    )
                }
                // Subscriptions Purchase Validation listener. Invoked after getting response from AppsFlyer servers
                // to let customer know if purchase was validated successfully
                .setSubscriptionValidationResultListener(object :
                    PurchaseClient.SubscriptionPurchaseValidationResultListener {
                    override fun onResponse(result: Map<String, SubscriptionValidationResult>?) {
                        result?.forEach { (k: String, v: SubscriptionValidationResult?) ->
                            if (v.success) {
                                Log.d(
                                    "TAG",
                                    "[PurchaseConnector]: Subscription with ID $k was validated successfully"
                                )
                                val subscriptionPurchase = v.subscriptionPurchase
                                Log.d("TAG", subscriptionPurchase.toString())
                            } else {
                                Log.d(
                                    "TAG",
                                    "[PurchaseConnector]: Subscription with ID $k wasn't validated successfully"
                                )
                                val failureData = v.failureData
                                Log.d("TAG", failureData.toString())
                            }
                        }
                    }

                    override fun onFailure(result: String, error: Throwable?) {
                        Log.d("TAG", "[PurchaseConnector]: Validation fail: $result")
                        error?.printStackTrace()
                    }
                })
                // In Apps Purchase Validation listener. Invoked after getting response from AppsFlyer servers
                // to let customer know if purchase was validated successfully
                .setInAppValidationResultListener(object :
                    PurchaseClient.InAppPurchaseValidationResultListener {
                    override fun onResponse(result: Map<String, InAppPurchaseValidationResult>?) {
                        result?.forEach { (k: String, v: InAppPurchaseValidationResult?) ->
                            if (v.success) {
                                Log.d(
                                    "TAG",
                                    "[PurchaseConnector]:  Product with Purchase Token$k was validated successfully"
                                )
                                val productPurchase = v.productPurchase
                                Log.d("TAG", productPurchase.toString())
                            } else {
                                Log.d(
                                    "TAG",
                                    "[PurchaseConnector]:  Product with Purchase Token $k wasn't validated successfully"
                                )
                                val failureData = v.failureData
                                Log.d("TAG", failureData.toString())
                            }
                        }
                    }

                    override fun onFailure(result: String, error: Throwable?) {
                        Log.d("TAG", "[PurchaseConnector]: Validation fail: $result")
                        error?.printStackTrace()
                    }
                })
                // Build the client
                .build()

            // Start the SDK instance to observe transactions.
            afPurchaseClient.startObservingTransactions()
        }

    }
}