package com.example.democamera.consent

import android.app.Activity
import android.content.Context
import androidx.fragment.app.Fragment
import com.example.democamera.consent.PreferencesManager

/**
 * Created by Felipe Frizeiro on 02/02/23.
 */

// region Internal Variables

internal val Activity.preferencesManager: PreferencesManager
    get() = PreferencesManager(this)

internal val Fragment.preferencesManager: PreferencesManager
    get() = PreferencesManager(requireContext())

internal val Context.preferencesManager: PreferencesManager
    get() = PreferencesManager(this)

// endregion
