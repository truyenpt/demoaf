package com.example.democamera.consent

import android.app.Activity
import android.content.Context
import android.preference.PreferenceManager
import android.util.Log
import androidx.annotation.Keep

import com.google.android.ump.ConsentDebugSettings
import com.google.android.ump.ConsentForm
import com.google.android.ump.ConsentInformation.ConsentStatus.*
import com.google.android.ump.ConsentRequestParameters
import com.google.android.ump.UserMessagingPlatform
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import java.lang.ref.WeakReference
import java.util.*

/**
 *
 */
@Keep
class ConsentManager(activity: Activity) {

    // region Companion Object

    companion object {
        private const val LAST_CONSENT_TIME = "PREF_LAST_CONSENT"
    }

    // endregion

    // region Public Variables

    val isApplicable: Boolean
        get() = consentInformation.consentStatus == OBTAINED || consentInformation.consentStatus == REQUIRED

    // endregion

    // region Private Variables

    private var activity: WeakReference<Activity> = WeakReference(activity)
    private val consentInformation by lazy { UserMessagingPlatform.getConsentInformation(activity) }
    private var consentInformationUpdated = false

    private val consentResult: Boolean
        get() = consentInformation.consentStatus == OBTAINED || consentInformation.consentStatus == NOT_REQUIRED

    private val consentRequestParameters: ConsentRequestParameters
        get() = consentBuilder().build()

    private val preferences: PreferencesManager?
        get() = activity.get()?.preferencesManager

    private val testDeviceIdList = mutableListOf<String>()

    // endregion

    // region Public Methods
//    fun getConsentResult(): Boolean {
//        return consentResult
//    }

    fun request(onConsentResult: (Boolean) -> Unit) {
        if (consentInformationUpdated && consentResult) {
            onConsentResult(consentResult)
            return
        }

        val activity = activity.get() ?: return onConsentResult(consentResult)

//        if (endOfConsentTime()) {
//            reset()
//        }

        consentInformation.requestConsentInfoUpdate(activity, consentRequestParameters, {
            consentInformationUpdated = true

            if (consentInformation.isConsentFormAvailable ) {
                loadForm(onConsentResult)
            } else {
                Log.e("TAG", "request: ", )
                Firebase.analytics.logEvent("CMP_FORM_NOT_AVAILABLE", null)
                onConsentResult(consentResult)
            }
        }, {
            Log.e("Consent", "requestConsentInfoUpdate.error: ${it.message}")
            Firebase.analytics.logEvent("CMP_FORM_ERROR", null)
            onConsentResult(consentResult)
        })
    }

    fun reset() {
        consentInformation.reset()
    }

    fun addTestDeviceId(id: String) {
        testDeviceIdList.add(id)
    }


    // endregion

    // region Private Methods

    private fun loadForm(onConsent: (Boolean) -> Unit) {
        Log.e("TAG", "loadForm: ", )
        val activity = activity.get() ?: return
        UserMessagingPlatform.loadAndShowConsentFormIfRequired(
            activity,
            ConsentForm.OnConsentFormDismissedListener { loadAndShowError ->
                if (canShowAds(activity)) {
                    Firebase.analytics.logEvent("CMP_Click_Consent", null)
                } else {
                    Firebase.analytics.logEvent("CMP_Not_Consent", null)
                }

                onConsent( canShowAds(activity))

            },
        )

    }

    private fun consentBuilder(): ConsentRequestParameters.Builder {
        val builder = ConsentRequestParameters.Builder()

        if (testDeviceIdList.size>0){
            val activity = activity.get() ?: return builder

            var testConsentGeography: ConsentGeography = ConsentGeography.EEA
            val debugBuilder = ConsentDebugSettings.Builder(activity)
                .setDebugGeography(testConsentGeography.debugGeography)
                .addTestDeviceHashedId("A861CCAC8CD37FBA38F29356780BEA4A")
                .apply {
                    testDeviceIdList.forEach { testId -> addTestDeviceHashedId(testId) }
                }


            builder.setConsentDebugSettings(debugBuilder.build())
        }

        return builder
    }

    private fun updateConsentTime() {
        preferences?.save(LAST_CONSENT_TIME, Date().time)
    }

    private fun endOfConsentTime(): Boolean {
        val last = preferences?.getLong(LAST_CONSENT_TIME) ?: activity.get()?.firstInstallTime ?: Date().time
        return last <= Date().add(-1, Calendar.YEAR).time
    }

    // endregion
    fun isGDPR(applicationContext: Context): Boolean {
        val prefs = PreferenceManager.getDefaultSharedPreferences(applicationContext)
        val gdpr = prefs.getInt("IABTCF_gdprApplies", 0)
        return gdpr == 1
    }

    fun canShowAds(applicationContext: Context): Boolean {
        val prefs = PreferenceManager.getDefaultSharedPreferences(applicationContext)

        //https://github.com/InteractiveAdvertisingBureau/GDPR-Transparency-and-Consent-Framework/blob/master/TCFv2/IAB%20Tech%20Lab%20-%20CMP%20API%20v2.md#in-app-details
        //https://support.google.com/admob/answer/9760862?hl=en&ref_topic=9756841

        val purposeConsent = prefs.getString("IABTCF_PurposeConsents", "") ?: ""
        val vendorConsent = prefs.getString("IABTCF_VendorConsents", "") ?: ""
        val vendorLI = prefs.getString("IABTCF_VendorLegitimateInterests", "") ?: ""
        val purposeLI = prefs.getString("IABTCF_PurposeLegitimateInterests", "") ?: ""

        val googleId = 755
        val hasGoogleVendorConsent = hasAttribute(vendorConsent, index = googleId)
        val hasGoogleVendorLI = hasAttribute(vendorLI, index = googleId)

        // Minimum required for at least non-personalized ads
        return hasConsentFor(listOf(1), purposeConsent, hasGoogleVendorConsent)
                && hasConsentOrLegitimateInterestFor(listOf(2, 7, 9, 10), purposeConsent, purposeLI, hasGoogleVendorConsent, hasGoogleVendorLI)

    }

    fun canShowPersonalizedAds(applicationContext: Context): Boolean {
        val prefs = PreferenceManager.getDefaultSharedPreferences(applicationContext)

        //https://github.com/InteractiveAdvertisingBureau/GDPR-Transparency-and-Consent-Framework/blob/master/TCFv2/IAB%20Tech%20Lab%20-%20CMP%20API%20v2.md#in-app-details
        //https://support.google.com/admob/answer/9760862?hl=en&ref_topic=9756841

        val purposeConsent = prefs.getString("IABTCF_PurposeConsents", "") ?: ""
        val vendorConsent = prefs.getString("IABTCF_VendorConsents", "") ?: ""
        val vendorLI = prefs.getString("IABTCF_VendorLegitimateInterests", "") ?: ""
        val purposeLI = prefs.getString("IABTCF_PurposeLegitimateInterests", "") ?: ""

        val googleId = 755
        val hasGoogleVendorConsent = hasAttribute(vendorConsent, index = googleId)
        val hasGoogleVendorLI = hasAttribute(vendorLI, index = googleId)

        return hasConsentFor(listOf(1, 3, 4), purposeConsent, hasGoogleVendorConsent)
                && hasConsentOrLegitimateInterestFor(listOf(2, 7, 9, 10), purposeConsent, purposeLI, hasGoogleVendorConsent, hasGoogleVendorLI)
    }

    // Check if a binary string has a "1" at position "index" (1-based)
    private fun hasAttribute(input: String, index: Int): Boolean {
        return input.length >= index && input[index - 1] == '1'
    }

    // Check if consent is given for a list of purposes
    private fun hasConsentFor(purposes: List<Int>, purposeConsent: String, hasVendorConsent: Boolean): Boolean {
        return purposes.all { p -> hasAttribute(purposeConsent, p) } && hasVendorConsent
    }

    // Check if a vendor either has consent or legitimate interest for a list of purposes
    private fun hasConsentOrLegitimateInterestFor(purposes: List<Int>, purposeConsent: String, purposeLI: String, hasVendorConsent: Boolean, hasVendorLI: Boolean): Boolean {
        return purposes.all { p ->
            (hasAttribute(purposeLI, p) && hasVendorLI) ||
                    (hasAttribute(purposeConsent, p) && hasVendorConsent)
        }
    }
}