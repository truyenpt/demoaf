package com.example.democamera.consent

import android.annotation.SuppressLint
import android.content.Context
import android.provider.Settings
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.Locale


/**
 * Created by CuongNK on 25/12/2023.
 */
object AppUtils {
    @SuppressLint("HardwareIds")
    fun getDeviceId(context: Context): String {
        return md5(Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)).uppercase(Locale.getDefault())
    }

    private fun md5(s: String): String {
        try {
            val digest = MessageDigest.getInstance("MD5")
            digest.update(s.toByteArray())
            val messageDigest = digest.digest()
            // Create Hex String
            val hexString: StringBuilder = StringBuilder()
            for (b in messageDigest) {
                val h = StringBuilder(Integer.toHexString(0xFF and b.toInt()))
                while (h.length < 2) h.insert(0, "0")
                hexString.append(h)
            }
            return hexString.toString()
        } catch (ignored: NoSuchAlgorithmException) {
        }
        return ""
    }
}