package com.example.democamera.consent

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences.Editor

/**
 * Created by Felipe Frizeiro on 02/02/23.
 */
internal class PreferencesManager(context: Context) {

    // region Private Variables

    private val prefs = context.getSharedPreferences("ADMOB_KIT_PREFS", MODE_PRIVATE)

    // endregion

    // region Public Methods

    fun save(key: String, value: Long) = save { it.putLong(key, value) }

    fun getLong(key: String) = prefs.getLong(key, -1L).let {
        if (it == -1L) null else it
    }
    fun getInt(key: String) = prefs.getInt(key, 0).let {
        if (it == 0) null else it
    }
    fun getString(key: String) = prefs.getString(key, "").let {
        if (it == "") null else it
    }
    // endregion

    // region Private Methods

    private fun save(saveHandler: (Editor) -> Unit) {
        val editor = prefs.edit()
        saveHandler(editor)
        editor.apply()
    }

    // endregion
}