package com.example.democamera

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.example.demoaf.BuildConfig
import com.example.demoaf.R
import com.example.democamera.consent.AppUtils
import com.example.democamera.consent.ConsentManager


class MainActivity : AppCompatActivity() {
    private var tvRecord: TextView? = null
    private val consentManager by lazy {
        ConsentManager(this).apply {
            if (BuildConfig.DEBUG) {
                val deviceID = AppUtils.getDeviceId(this@MainActivity)
                addTestDeviceId(deviceID)
            }
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        tvRecord = findViewById(R.id.tvRecord)
        consentManager.reset()
        consentManager.request {
         // funtion next màn
        }

    }


}